import json
from thorin import State

f = open('../config.json', 'r')
config = json.loads(f.read())

states = {}
for state_cfg in config['states']:
    states[state_cfg['name']] = State(state_cfg)

assert(states['R0'] != None)
assert(states['R0'].next_state[0]['condition'] == 't == 0')
assert(states['R0'].next_state[0]['goto'] == 'Q1')

assert(states['Q1'] != None)
assert(states['Q1'].next_state[0]['goto'] == 'self' or states['Q1'].next_state[1]['goto'] == 'self')

print("Passed")
