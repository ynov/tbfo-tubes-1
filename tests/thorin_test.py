from thorin import Thorin

thr = Thorin()
assert(thr.t == 0)

for i in range(10):
    thr.incr_time()
assert(thr.t == 10)

thr.reset_time()
assert(thr.t == 0)

assert(thr.start_state == 'R0')
assert(thr.lights != None)

assert(thr.states['R0'] != None)
assert(thr.states['Q1'] != None)

thr.incr_state()
assert(thr.current_states[0].name == 'Q1')

for i in range(120):
    thr.incr_state()

assert(thr.t == 120)

thr.incr_state()
assert(thr.current_states[0].name == 'Q2')

for current_state in thr.current_states:
    print(current_state.name)

print("Passed")
