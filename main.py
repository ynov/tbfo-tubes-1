import time
from thorin import Thorin

th = Thorin('./config.json')

def print_lights(th):
    l = """
            #     '     #
            #     '     #
            #     '  {4}  #
#############~~~~~~~~~~~###############
          {0} !           !
----------- !           ! -------------
            !     X     ! {2}
- - - - - - !           ! - - - - - - -
            !           ! {1}
#############~~~~~~~~~~~~##############
            #  {3}  '     #
            #     '     #
            #     '     #
""".format(th.lights['T1'], th.lights['T2A'], th.lights['T2B'], th.lights['T3'], th.lights['T4'])
    print(l)

t = 0
while True:
    t += 1
    print("Time Elapsed: {0} time unit".format(t))

    th.incr_state()
    print_lights(th)
    time.sleep(0.15)
