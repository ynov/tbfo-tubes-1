import json
from thorin import State

class Thorin:
    def __init__(self, config_file='../config.json'):
        self.config_file = config_file
        self.current_states = []

        self.start_state = None
        self.end_states  = []

        self.states = {}
        self.t      = 0

        self.lights = {
            'T1':  'R',
            'T2A': 'R',
            'T2B': 'R',
            'T3':  'R',
            'T4':  'R'
        }

        self.load_config()

    def load_config(self):
        f = open(self.config_file, 'r')
        config = json.loads(f.read())

        # Populate states
        for state_cfg in config['states']:
            self.states[state_cfg['name']] = State(state_cfg)

        self.start_state = config['start_state']
        self.end_states  = config['end_states']

        self.current_states.append(self.states[self.start_state])

    def reset_time(self):
        self.t = 0

    def incr_time(self):
        self.t += 1

    def incr_state(self, input = None):
        for state in self.current_states:
            advance = False

            for ns in state.next_state:
                condition = ns['condition']
                goto      = ns['goto']
                do        = ns['do']

                if input != None:
                    pass

                elif 't == ' in condition:
                    val = (int(condition[len('t == '):]))

                    if do == 'reset_time':
                        self.reset_time()

                    if self.t == val:
                        self.current_states.append(self.states[goto])
                        self.set_lights(self.states[goto])
                        advance = True

                elif 't < ' in condition:
                    val = (int(condition[len('t < '):]))
                    if self.t < val:
                        if goto == 'self':
                            if do == 'incr_time':
                                self.incr_time()
                                advance = False
            if advance:
                self.current_states.remove(state)

    def set_lights(self, state):
        if state.lights != None:
            self.lights = state.lights
