class State:
    def __init__(self, state_cfg):
        self.name        = state_cfg['name']
        self.description = state_cfg['description']
        self.lights      = state_cfg['lights']
        self.next_state  = state_cfg['next_state']

    def __str__(self):
        return {
            'name': self.name, 'description': self.description,
            'lights': self.lights, 'next_state': self.next_state
        }.__str__()

    def __repr__(self):
        return self.__str__()
